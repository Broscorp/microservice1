package com.acme.microservice1.filter;

import static java.lang.String.format;

import java.io.IOException;
import java.util.UUID;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
@Log4j2
@Order(2)
public class RequestIdentityLoggerFilter extends OncePerRequestFilter {

  private static final String MICROSERVICE_ID = HostnameUtil.getHostname();

  @Override
  protected void doFilterInternal(
      final HttpServletRequest request, final HttpServletResponse response, final FilterChain chain)
      throws IOException, ServletException {

    final String uuid = UUID.randomUUID().toString();
    final String method = request.getMethod();
    final String uri = request.getRequestURI();

    logger.info(
        format(
            "uuid: %s: at microservice %s queried for %s %s", uuid, MICROSERVICE_ID, method, uri));
    chain.doFilter(request, response);
  }
}
