package com.acme.microservice1.filter;

import java.util.List;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@ControllerAdvice
public class UuidResponseAdvice implements ResponseBodyAdvice {

  private static final String HEADER_VALUE = "ms1-" + HostnameUtil.getHostname() +"/%s";
  private static final String MS_HEADER_NAME = "X-Microservice-Flow";

  @Override
  public boolean supports(MethodParameter returnType, Class converterType) {
    return true;
  }

  @Override
  public Object beforeBodyWrite(
      Object body,
      MethodParameter returnType,
      MediaType selectedContentType,
      Class selectedConverterType,
      ServerHttpRequest request,
      ServerHttpResponse response) {
    String currentHeaderValue = response.getHeaders().getFirst(MS_HEADER_NAME);
    response.getHeaders().clear();
    response
        .getHeaders()
        .put(MS_HEADER_NAME, List.of(String.format(HEADER_VALUE, currentHeaderValue)));
    return body;
  }
}
