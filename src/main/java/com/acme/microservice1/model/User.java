package com.acme.microservice1.model;

import lombok.Data;

@Data
public class User {

  private Long id;
  private String fullName;
  private String email;
}
