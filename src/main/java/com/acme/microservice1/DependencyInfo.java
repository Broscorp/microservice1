package com.acme.microservice1;

import java.nio.file.Files;
import java.nio.file.Paths;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

@Component
public class DependencyInfo implements InfoContributor {

  @Override
  public void contribute(Info.Builder builder) {
    try {
      String out =
          String.join(
              "\n",
              Files.readAllLines(
                  Paths.get(ClassLoader.getSystemResource("dependency-list").toURI())));
      builder.withDetail("dependencies", out);
    } catch (Exception e) {
      builder.withDetail("dependencies", "couldn't find dependency list");
    }
  }
}
