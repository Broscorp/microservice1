package com.acme.microservice1.controller;

import com.acme.microservice1.model.User;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping(value = {"/data"})
public class DataController {

  private final RestTemplate restTemplate;

  @Value("${microservice.url}/data")
  private String url;

  public DataController(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @PostMapping
  public ResponseEntity<User> saveUser(@RequestBody User user) {
    return restTemplate.postForEntity(url, user, User.class, new HashMap<>());
  }

  @GetMapping
  public ResponseEntity<Iterable<User>> findAll() {
    return restTemplate.exchange(
        UriComponentsBuilder.fromHttpUrl(url).toUriString(),
        HttpMethod.GET,
        null,
        new ParameterizedTypeReference<>() {});
  }

  @GetMapping("/{id}")
  public ResponseEntity<User> findUserById(@PathVariable long id) {
    return restTemplate.getForEntity(
        UriComponentsBuilder.fromHttpUrl(url).pathSegment(String.valueOf(id)).toUriString(),
        User.class);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<User> deleteUserById(@PathVariable long id) {
    return restTemplate.exchange(
        UriComponentsBuilder.fromHttpUrl(url).pathSegment(String.valueOf(id)).toUriString(),
        HttpMethod.DELETE,
        null,
        User.class);
  }
}
